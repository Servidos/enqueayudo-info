<?php
session_start();

?>


<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Formulario de voluntariado">
  <meta name="author" content="Felipe Borja Pérez">
  <meta name="theme-color" content="#b2129c">
  <title>#ENQUÉAYUDO - formulario</title>


  <link href="css/bootstrap.css" rel="stylesheet" crossorigin="anonymous">
  <!--custom-->
  <link href="css/custom.css" rel="stylesheet">
  <link rel="icon" type="image/gif" href="favicon.png">
  <!--imports-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body id="body" >
<nav class="navbar navbar-expand-lg navbar-custom fixed-top barra-principal  " style="max-width: 100%">
  <a class="navbar-brand letra-titulo" style=""  href="#">
    <p class="h2 celeste" style="display: inline"> #</p>
    <p class="h2 " style="display: inline">EN</p>
    <p class="h2 celeste" style="display: inline">QUÉ</p>
    <p class="h1 "  style="display: inline">AYUDO</p>
    <p class="h1 "  style="display: inline"> ? </p> </a>
  <button class="navbar-toggler border-info navbar-dark" type="button" data-toggle="collapse" data-target="#navegacion"
          aria-controls="navegacion" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"> </span>
  </button>
  <div class="navbar-collapse collapse letra-titulo" id="navegacion">
    <ul class="navbar-nav nav-pills mr-auto letra-nav d-flex justify-content-between">
      <li class="nav-item " >
        <a class="nav-link "  href="/index.html">INICIO <div class="divisor vertical-center"></div></a>
      </li>
      <li class="nav-item ">
        <a class="nav-link " href="/bioseguridad.html"> BIOSEGURIDAD<div class="divisor vertical-center"></div></a>
      </li>
      <li class="nav-item ">
        <a class="nav-link " href="/privacy.html"> TÉRMINOS Y CONDICIONES <div class="divisor vertical-center"></div></a>
      </li>
      <li class="nav-item ">
        <a class="nav-link " href="#pie">CONTACTOS</a>
      </li>
    </ul>
  </div>
</nav>

<main role="main" class="container " >
  <div class="row text-center "  >
    <div class=" col-md-12 bg-light " style="margin: 20px;height: auto">
      <h1 class="texto-grande text-uppercase morado" style="margin-top: 2rem">Formulario</h1>
        <div class="col-md-8 centro canaro texto-decripcion" >
            <?php
        if(isset($_SESSION['ERROR'])) { ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php echo $_SESSION['ERROR']; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php }
        if(isset($_SESSION['CORRECTO'])) { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                Voluntario agregado correctamente
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php  }
            unset($_SESSION['CORRECTO']);
            unset($_SESSION['ERROR']);
            session_destroy();
        ?>
        </div>


      <p class="texto-decripcion centro" style="font-weight: normal" >
        Este formulario tiene como objetivo recopilar información personal y confidencial para levantar una ficha individual de voluntario (procesamiento neto de salud) para evitar su sobreexposición frente al COVID19 . Valoramos y felicitamos su loable labor y predisposición pero es nuestra obligación darle seguimiento a usted de manera que disminuyamos el riesgo y propagación de contagio . Es importante que también estemos al tanto de su nivel de instrución y/o capacitación entorno a MEDIDAS BÁSICAS DE  BIOSEGURIDAD y de COVID19  para poder acompañar su trabajo de manera que el impacto sea positivo tanto para usted cómo para los beneficiarios.
      </p>
      <form id="form-voluntario" class="roboto" style="height: auto" action="enviar.php" method="post" accept-charset="latin1">
        <div class="row ">
        <div class="form-group col-md-4 voluntario-container">
          <label class=" " for="vol-nombre" hidden></label>
          <!--span class="fa fa-user morado form-control-feedback" ></--span -->
          <input type="text" name="nombres" id="vol-nombre" style="padding: 5px"
                 class="form-control mail-border " required placeholder="NOMBRE COMPLETO">
        </div>
        <div class="form-group col-md-4 voluntario-container">
          <label class=" " for="vol-cedula" hidden></label>

          <input type="number" name="cedula" id="vol-cedula" style="padding: 5px" min="100000000" max="2499999999"
                 class="form-control mail-border " required placeholder="CEDULA">
        </div>
        <div class="form-group col-md-4 voluntario-container">
          <label class=" " for="vol-numero" hidden></label>

          <input type="number" name="numero" id="vol-numero" style="padding: 5px" min="20000000" max="999999999"
                 class="form-control mail-border " required placeholder="TELÉFONO">
        </div>
        <div class="form-group col-md-4 voluntario-container">
          <label class=" " for="vol-mail" hidden></label>

          <input type="email" name="correo" id="vol-mail" style="padding: 5px"
                 class="form-control mail-border " required placeholder="USUARIO@CORREO.COM">
        </div>
        <div class="form-group col-md-4 voluntario-container">
          <label class=" " for="vol-edad" hidden></label>

          <input type="number" name="edad" id="vol-edad" style="padding: 5px" min="17" max="99"
                 class="form-control mail-border " required placeholder="EDAD">
        </div>
        <div class="form-group col-md-4 voluntario-container">
          <label class=" " for="vol-ciudad" hidden></label>
          <input type="text" name="ciudad" id="vol-ciudad" style="padding: 5px"
                 class="form-control mail-border " required placeholder="CIUDAD">
        </div>
        </div> <!--row-->

        <div class="row "  style="margin: 2rem">
          <div class="form-group col-md-6 voluntario-container">
            <h3><label class=" " for="vol-preg-12" >
              ¿es usted dirigente barrial o pertenece a alguna organización social?
            </label></h3>
            <div class="row" style="margin: 1rem">
              <select id="vol-preg-12" name="vol-preg-12" style="padding: 5px"
                      class="form-control col-md-2 centro">
                <option id="p12-op1" selected value="si">Si</option>
                <option id="p12-op2" value="no">No</option>
              </select>
              <label class=" " for="vol-preg-12s" hidden></label>
              <input type="text" name="vol-preg-12s" id="vol-preg-12s"  style="padding: 5px;margin-left: 1rem"
                     class="form-control mail-border col-md-8"  placeholder="Especifique cual">
            </div>
          </div>
            <div class="form-group col-md-6 voluntario-container">
                <h3><label class=" " for="vol-preg-11" >
                        ¿Usted convive con alguna persona mayores de 65 años o que tenga alguna enfermedad de importancia?
                    </label></h3>
                <select id="vol-preg-11" name="vol-preg-11" style="padding: 5px"
                        class="form-control col-md-2 centro">
                    <option value="si" selected>Si</option>
                    <option value="no">No</option>
                </select>
            </div>
        <div class="form-group col-md-6 voluntario-container">
          <h3><label class=" " for="vol-preg-1" >
            En los últimos 15 días ¿ha tenido contacto con personas de otra provincia?
          </label></h3>
          <div class="row" style="margin: 1rem">
            <select id="vol-preg-1" name="vol-preg-1" style="padding: 5px" class="form-control col-md-2 centro">
              <option id="p1-op1" value="si" selected>Si</option>
              <option id="p1-op2" value="no">No</option>
            </select>
            <label class=" " for="vol-preg-1s" hidden></label>
            <input type="text" name="vol-preg-1s" id="vol-preg-1s" style="padding: 5px;margin-left: 1rem"
                   class="form-control mail-border col-md-8"  placeholder="Especifique cual">
          </div>
        </div>
        <div class="form-group col-md-6 voluntario-container">
          <h3><label class=" " for="vol-preg-2" >
            En los últimos 15 días ¿usted ha salido de la provincia?
          </label></h3>
          <div class="row" style="margin: 1rem">
            <select id="vol-preg-2" name="vol-preg-2" style="padding: 5px" class="form-control col-md-2 centro">
              <option id="p2-op1" value="si" selected>Si</option>
              <option id="p2-op2" value="no">No</option>
            </select>
            <label class=" " for="vol-preg-2s" hidden></label>
            <input type="text" name="vol-preg-2s" id="vol-preg-2s" style="padding: 5px;margin-left: 1rem"
                   class="form-control mail-border col-md-8 "  placeholder="Especifique cual">
          </div>
        </div>
        <div class="form-group col-md-6 voluntario-container">
          <h3><label class=" " for="vol-preg-3" >
            En los últimos 15 días ¿ha tenido contacto con personas que han tenido fiebre, gripe, tos o dificultad para respirar?
          </label></h3>
          <div class="row" style="margin: 1rem">
            <select id="vol-preg-3" name="vol-preg-3" style="padding: 5px" class="form-control col-md-2 centro">
              <option value="si" selected>Si</option>
              <option value="no">No</option>
            </select>
          </div>
        </div>
        <div class="form-group col-md-6 voluntario-container">
          <h3><label class=" " for="vol-preg-4" >
            En los últimos 15 días ¿usted ha tenido fiebre, gripe, tos o dificultad para respirar?
          </label></h3>
            <select id="vol-preg-4" name="vol-preg-4" style="padding: 5px" class="form-control col-md-2 centro">
              <option value="si" selected>Si</option>
              <option value="no">No</option>
            </select>
        </div>
        <div class="form-group col-md-6 voluntario-container">
          <h3><label class=" " for="vol-preg-5" >
            ¿Usted actualmente toma algún medicamento?
          </label></h3>
          <div class="row" style="margin: 1rem">
            <select id="vol-preg-5" name="vol-preg-5" style="padding: 5px" class="form-control col-md-2 centro">
              <option id="p5-op1" value="si" selected>Si</option>
              <option id="p5-op2" value="no">No</option>
            </select>
            <label class=" " for="vol-preg-5s" hidden></label>
            <input type="text" name="vol-preg-5s" id="vol-preg-5s"  style="padding: 5px;margin-left: 1rem"
                   class="form-control mail-border col-md-8"  placeholder="Especifique cual">
          </div>
        </div>
        <div class="form-group col-md-6 voluntario-container">
          <h3><label class=" " for="vol-preg-6" >
            ¿Usted padece alguna enfermedad de importancia?
          </label></h3>
          <div class="row" style="margin: 1rem">
            <select id="vol-preg-6" name="vol-preg-6" style="padding: 5px"
                    class="form-control col-md-6 centro">
              <option id="p6-op1" value="Hipertensión">Hipertensión</option>
              <option id="p6-op2" value="Diabetes" >Diabetes</option>
              <option id="p6-op3" value="Cáncer">Cáncer</option>
              <option id="p6-op4" value="Hipertiroidismo">Hipertiroidismo</option>
              <option id="p6-op5" value="Hipotiroidismo">Hipotiroidismo</option>
              <option id="p6-op6" value="Enfermedad crónica">Enfermedad crónica</option>
              <option selected id="p6-op7" value="Ninguna">Ninguna</option>
              <option id="p6-op8" value="Otra">Otra</option>
            </select>
            <label class=" " for="vol-preg-6s" hidden></label>
            <input type="text" name="vol-preg-6s" id="vol-preg-6s" style="padding: 5px;margin-left: 0"
                   class="form-control mail-border col-md-6"  placeholder="Especifique" hidden>
          </div>
        </div>

        <div class="form-group col-md-6 voluntario-container">
          <h3><label class=" " for="vol-preg-7" >
            ¿Usted es personal de salud?
          </label></h3>
          <select id="vol-preg-7" name="vol-preg-7" style="padding: 5px"
                  class="form-control col-md-2 centro">
            <option id="p7-op1" value="si" >Si</option>
            <option id="p7-op2" value="no" selected>No</option>
          </select>
        </div>

        <div class="form-group col-md-6 voluntario-container" id="preg-medico-1" hidden>
          <h3><label class=" " for="vol-preg-8" >
            ¿Cuál es su rol en el área de salud?
          </label></h3>
          <div class="row" style="margin: 1rem">
            <select id="vol-preg-8" name="vol-preg-8" style="padding: 5px"
                    class="form-control col-md-6 centro">
              <option selected id="p8-op1" value="Médico">Médico</option>
              <option id="p8-op2" value="Paramédico">Paramédico</option>
              <option id="p8-op3" value="Enfermera/o">Enfermera/o</option>
              <option id="p8-op4" value="Laboratorista">Laboratorista</option>
              <option id="p8-op5" value="Terapia física">Terapia física</option>
              <option id="p8-op6" value="Otro">Otro</option>
            </select>
            <label class=" " for="vol-preg-8s" hidden></label>
            <input type="text" name="vol-preg-8s" id="vol-preg-8s" style="padding: 5px;margin-left: 0"
                   class="form-control mail-border col-md-6"  placeholder="Especifique" hidden>
          </div>
        </div>
        <div class="form-group col-md-6 voluntario-container" id="preg-medico-2" hidden>
          <h3><label class=" " for="vol-preg-9" >
            ¿En los últimos 15 días usted ha trabajado directamente en la atención o vigilancia de pacientes sospechosos o diagnosticados con n covid-19?
          </label></h3>
          <select id="vol-preg-9" name="vol-preg-9" style="padding: 5px"
                  class="form-control col-md-2 centro">
            <option value="si" selected>Si</option>
            <option value="no">No</option>
          </select>
        </div>
        <div class="form-group col-md-6 voluntario-container" id="preg-medico-3" hidden>
          <h3><label class=" " for="vol-preg-10" >
            ¿En los últimos 2 meses ha realizado algún curso sobre el nuevo coronavirus y bioseguridad?
          </label></h3>
          <div class="row" style="margin: 1rem">
            <select id="vol-preg-10" name="vol-preg-10" style="padding: 5px"
                    class="form-control col-md-2 centro">
              <option id="p10-op1" value="si" selected>Si</option>
              <option id="p10-op2" value="no">No</option>
            </select>
            <label class=" " for="vol-preg-10s" hidden></label>
            <input type="text" name="vol-preg-10s" id="vol-preg-10s"  style="padding: 5px;margin-left: 1rem"
                   class="form-control mail-border col-md-8"  placeholder="Especifique cual">
          </div>

        </div>
      </div> <!--row-->
        <div class="row" style="margin: 2rem">
        <div class=" col-md-12 ">
          <div class="row ">
            <div class="form-group col-md-6 centro">
              <button type="submit" class="btn btn-outline-info bg-boton form-boton">ENVIAR FORMULARIO</button>
            </div>
          </div>
        </div>
      </div> <!--row-->

      </form>

    <div class="barra-pequeña" style="z-index: 10"></div>
    <div class="barra-intermedia" style="z-index: 10"></div>
    <div class="barra-pequeña" style="z-index: 10"></div>
    </div>
  </div><!-- /.row -->
</main><!-- /.container -->

<footer id="pie" class="page-footer font-weight-normal container" style="height: 360px">
  <div class="row">
    <div class="col-md-6 bg-transparent">
      <form class="row" style="height: 80%" action="js/mail.php" method="post">
        <div class="form-group col-md-6 inscribete-container">
          <label class=" " for="form-nombre" hidden></label>
          <span class="fa fa-user morado form-control-feedback" ></span>
          <input type="text" name="nombres" id="form-nombre" class="form-control mail-border " required placeholder="NOMBRES Y APELLIDOS">
        </div>
        <div class="form-group col-md-6 inscribete-container">
          <label class=" " for="form-mail" hidden></label>
          <span class="fa fa-envelope morado form-control-feedback" ></span>
          <input type="email" name="email" id="form-mail" class="form-control mail-border "   required placeholder="USUARIO@CORREO.COM">
        </div>
        <div class="form-group col-md-6 inscribete-container">
          <label class=" " for="form-dire" hidden></label>
          <span class="fa fa-map morado form-control-feedback"  ></span>
          <textarea  rows="2" name="direccion" id="form-dire" class="form-control mail-border "  required placeholder="DIRECCIÓN"></textarea>
        </div>
        <div class="form-group col-md-6 inscribete-container">
          <label class=" " for="form-soli" hidden></label>
          <span class="fa fa-sticky-note morado form-control-feedback"  ></span>
          <textarea  rows="2" name="mensaje" id="form-soli" class="form-control mail-border "
                     style="padding: auto" placeholder="MENSAJE, RECOMENDACIÓN O DENUNCIA"  required></textarea>
        </div>
        <div class="form-group col-md-5 centro">
          <button type="submit" class="btn btn-outline-info bg-boton form-boton">ENVIAR</button>
        </div>
      </form>

      <div class="row logo-redes-container"  >
        <a href="https://www.facebook.com/enqueayudo" >
          <img class="logo-redes" alt="" src="assets/link_face.png" />
        </a>
        <a href="https://twitter.com/enqueayudo" >
          <img class="logo-redes" alt="" src="assets/link_tw.png" />
        </a>
      </div>
    </div>
    <div class="col-md-6 bg-white"  >
      <img class="logo-footer" alt="" src="assets/logo_footer.jpg" />
    </div>
  </div>
</footer>

<script src="js/jquery-3.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src=""><\/script>')</script><script src="js/bootstrap.js" crossorigin="anonymous"></script>

<script>

  $(document).ready(function () {
    $('#vol-preg-1').change(function () {
      if ($(this).val() === 'si') {
        $('#vol-preg-1s').prop('hidden', false);
      }else{
        $('#vol-preg-1s').prop('hidden', true);
      }
    });
    $('#vol-preg-2').change(function () {
      if ($(this).val() === 'si') {
        $('#vol-preg-2s').prop('hidden', false);
      }else{
        $('#vol-preg-2s').prop('hidden', true);
      }
    });
    $('#vol-preg-5').change(function () {
      if ($(this).val() === 'si') {
        $('#vol-preg-5s').prop('hidden', false);
      }else{
        $('#vol-preg-5s').prop('hidden', true);
      }
    });
    $('#vol-preg-6').change(function () {
      if ($(this).val() === 'Otra') {
        $('#vol-preg-6s').prop('hidden', false);
      }else{
        $('#vol-preg-6s').prop('hidden', true);
      }
    });

    $('#vol-preg-7').change(function () {
      if ($(this).val() === 'si') {
        $('#preg-medico-1').prop('hidden', false);
        $('#preg-medico-2').prop('hidden', false);
        $('#preg-medico-3').prop('hidden', false);
      }else{
        $('#preg-medico-1').prop('hidden', true );
        $('#preg-medico-2').prop('hidden', true );
        $('#preg-medico-3').prop('hidden', true );
      }
    });
    $('#vol-preg-8').change(function () {
      if ($(this).val() === 'Otro') {
        $('#vol-preg-8s').prop('hidden', false);
      }else{
        $('#vol-preg-8s').prop('hidden', true);
      }
    });
    $('#vol-preg-10').change(function () {
      if ($(this).val() === 'si') {
        $('#vol-preg-10s').prop('hidden', false);
      }else{
        $('#vol-preg-10s').prop('hidden', true);
      }
    });
    $('#vol-preg-12').change(function () {
      if ($(this).val() === 'si') {
        $('#vol-preg-12s').prop('hidden', false);
      }else{
        $('#vol-preg-12s').prop('hidden', true);
      }
    });
  });

</script>

<script src="js/os.js"></script>
<script >
  // <link href="js/os.js" rel="script">
  let os = getOS() ;
  if (os==="Windows") {
    document.getElementById("body").style.fontFamily = "'AGENCY FB',sans-serif";
    //alert("Windows");
  } else {

    //alert("not windows");
  }
</script>
</body>
</html>
