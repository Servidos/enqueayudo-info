<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once("../db.php");
$x =  array(
    mysqli_real_escape_string($conn, $_POST['nombres']),
    mysqli_real_escape_string($conn, $_POST['cedula']),
    mysqli_real_escape_string($conn, $_POST['numero']),
    mysqli_real_escape_string($conn, $_POST['correo']),
    mysqli_real_escape_string($conn, $_POST['edad']),
    mysqli_real_escape_string($conn, $_POST['ciudad']),
    mysqli_real_escape_string($conn, $_POST['vol-preg-1']),
    mysqli_real_escape_string($conn, $_POST['vol-preg-1s']),
    mysqli_real_escape_string($conn, $_POST['vol-preg-2']),
    mysqli_real_escape_string($conn, $_POST['vol-preg-2s']),
    mysqli_real_escape_string($conn, $_POST['vol-preg-3']),
    mysqli_real_escape_string($conn, $_POST['vol-preg-4']),
    mysqli_real_escape_string($conn, $_POST['vol-preg-5']),
    mysqli_real_escape_string($conn, $_POST['vol-preg-5s']),
    mysqli_real_escape_string($conn, $_POST['vol-preg-6']),
    mysqli_real_escape_string($conn, $_POST['vol-preg-6s']),
    mysqli_real_escape_string($conn, $_POST['vol-preg-7']),
    mysqli_real_escape_string($conn, $_POST['vol-preg-8']),
    mysqli_real_escape_string($conn, $_POST['vol-preg-8s']),
    mysqli_real_escape_string($conn, $_POST['vol-preg-9']),
    mysqli_real_escape_string($conn, $_POST['vol-preg-10']),
    mysqli_real_escape_string($conn, $_POST['vol-preg-10s']),
    mysqli_real_escape_string($conn, $_POST['vol-preg-11']),
    mysqli_real_escape_string($conn, $_POST['vol-preg-12']),
    mysqli_real_escape_string($conn, $_POST['vol-preg-12s'])
);
//not null
$error = 0;
$errorTXT="";
foreach ($x as $n) {
    if ($n === null){
        $error = 25;
        break;
    }
    if ($n=== ""){
        $error+=1;
    }
}
if ($error<8) {

    $sql = "SELECT cedula FROM formulario WHERE cedula='$x[1]'";
    $result = $conn->query($sql);

    if ($result->num_rows == 0) { //si la cedula no existe
        $sql2 = "INSERT INTO formulario VALUES ('$x[0]','$x[1]','$x[2]','$x[3]','$x[4]',
    '$x[5]','$x[6]','$x[7]','$x[8]','$x[9]','$x[10]','$x[11]','$x[12]','$x[13]','$x[14]',
    '$x[15]','$x[16]','$x[17]','$x[18]','$x[19]','$x[20]','$x[21]','$x[22]','$x[23]','$x[24]')";
        if ($conn->query($sql2) !== TRUE) {
            $errorTXT= "Error al ingresar, intente de nuevo más tarde";
        }
    } else {
        $errorTXT= "La cédula ya se encuentra ingresada";
    }
}else{
    $errorTXT= "El formulario es incorrecto";
}

if ($errorTXT===""){
    $_SESSION['CORRECTO'] = "sif";
}else{
    $_SESSION['ERROR'] = $errorTXT;
}

header('Location: ' .'../formulario.php');
exit();

/*
TODO enviar correo

    $direcciones = array(
        "info@enqueayudo.info",
        "legal@enqueayudo.info",
        "soporte@enqueayudo.info"
    );
    $to = implode(',', $direcciones);
    $subject = "Enqueayudo Formulario de voluntario: ".$x[3] ;
    $headers = "From: admin@enqueayudo.info";
    $message = "
        Enqueayudo: Formulario de voluntario \r\n \r\n
        Nombre: $f[0]  \r\n
        Cedula $f[1]  \r\n
        Numero $f[2]  \r\n
        Correo $f[3]  \r\n
        Edad: $f[4]  \r\n
        Ciudad: $f[5]  \r\n\n
        Preguntas:  \r\n
        1: $p[0] $p[1] \r\n
        2: $p[2] $p[3]  \r\n
        3: $p[4]   \r\n
        4: $p[5]    \r\n
        5: $p[6] $p[7]  \r\n
        6: $p[8] $p[9]  \r\n
        7: $p[10]   \r\n
        8: $p[11] $p[12]    \r\n
        9: $p[13]   \r\n
        10: $p[14] $p[15]   \r\n
        11: $p[16]  \r\n
        12: $p[17] $p[18]   
       ";
    if ($f[2]!==""){
        mail($to,$subject,$message,$headers);
    }

    header('Location: ' .'../formulario.html');
    die();
    */


?>
